from setuptools import setup, find_packages

setup(
    name="sms4you",
    version="0.0.1",
    description="Simple gateway for using SMS with other means of communication",
    long_description="Simple gateway for using SMS with other means of communication",
    url="https://gitlab.com/xamanu/sms4you",
    license="AGPLv3+",
    keywords="sms email gateway",
    author="Various collaborators: https://gitlab.com/xamanu/sms4you",
    install_requires=["dotenv", "gammu", "sdnotify"],
    packages=find_packages(),
    include_package_data=True,
    entry_points={"console_scripts": ["sms4you = sms4you.sms4you:main"]},
)
