# sms4you-xc - XMPP component for SMS

While sms4you is a gateway between email and SMS, this little
subproject, named `sms4you-xc`, is a gataway between SMS and XMPP. It
is different from sms4you in two important aspects:

* it runs as an XMPP server component, i.e. you need your own XMPP
  server
* it uses [ModemManager][mm] instead of Gammu for accessing the modem

If you are interested in an XMPP-SMS gateway, but do not like to
maintain your own XMPP server, please take a look at [JMP][jmp].

## Requirements

* An XMPP server must be running on the system, e.g. [Ejabberd][ejabberd] or
  [Prosody][prosody], which at least has s2s connection to the outside world
* ModemManager must be running on the system, it will be accessed via
  DBus and your modem must, of course, be supported by ModemManager
* [Python 3][python]
* [`python3-dbussy`][dbussy]
* [`python3-sdnotify`][sdnotify]
* [`python3-slixmpp`][slixmpp]

## Installation
* `sudo cp xmpp-component/sms4you-xc /usr/local/sbin/`
* `sudo cp xmpp-component/sms4you-xc.service /etc/systemd/system/`
* `sudo mkdir /etc/sms4you/`

## Configuration

### Ejabberd

**TODO**

### Prosody

Add the following lines to your prosody configuration,
e.g. `/etc/prosody/prosody.cfg.lua`:

    Component "sms4you.localhost"
        component_secret = "Mb2.r5oHf-0t"

### sms4you-xc

Save the password (`Mb2.r5oHf-0t`) in
`/etc/sms4you/xmpp_component_password.txt`.  and configure the XMPP
account (JID), which should be allowed to send and receive SMS in
`/etc/sms4you/xmpp_component.env`:

    USER_JID=user@jabberserver.foo.bar
	
You probably need to configure more details in
`/etc/systemd/system/sms4you-xc.service`. Look at the command line
arguments: `/usr/local/sbin/sms4you-xc --help`.

## Let's Go

* `sudo systemctl enable sms4you-xc.service`
* `sudo systemctl start sms4you-xc.service`

## Future

Some improvement ideas:
* support multiple modems, each with its own associated users
* optionally send SMS to MUC, MIX or PubSub
* restrictions for sending SMS, e.g. no international SMS or limits
  per month

### Copyright and copyleft

Copyright (C) 2019  W. Martin Borgert <debacle@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Also add information on how to contact you by electronic and paper mail.

If your software can interact with users remotely through a computer
network, you should also make sure that it provides a way for users to
get its source.  For example, if your program is a web application, its
interface could display a "Source" link that leads users to an archive
of the code.  There are many ways you could offer source, and different
solutions will be better for different programs; see section 13 for the
specific requirements.

See the [LICENSE.md](../LICENSE.md) for the full license text.

[dbussy]:https://github.com/ldo/dbussy
[ejabberd]:https://www.ejabberd.im/
[jmp]:https://jmp.chat/
[mm]:https://www.freedesktop.org/wiki/Software/ModemManager/
[prosody]:https://www.prosody.im/
[python]:https://www.python.org
[sdnotify]:https://github.com/bb4242/sdnotify
[slixmpp]:https://lab.louiz.org/poezio/slixmpp
