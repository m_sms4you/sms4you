# sms4you

Simple gateway for using SMS with other means of communication.
Currently email is supported and the tool will be extended to other protocols such as xmpp and matrix.

![sms4you on a Raspberry Pi](./sms4you-raspberry-pi.jpg)

## Idea

Connect a [suitable GSM modem or phone](https://wammu.eu/phones/) containing a SIM card into a computer (e.g. a Raspberry Pi) at a fixed place. Give `sms4you` access (imap and smtp) to one dedicated email address. It will use this connection to receive emails to be sent out as SMS and to send out emails with received SMS messages. The phone number will be managed over the email's subject and only emails are accepted coming from a certain email address.

## Concretely - why?

There can be many reasons, why you want to use `sms4you`. Here are some examples:

* You are living in the internetz, and you just need a gateway to this old thing "SMS", which some people really still seem to use.
* You travel and use local SIM cards, but you still want to be able to receive SMS confirmation codes from banks and services.
* You don't want to carry a (registered) SIM card for [good reasons](https://www.theguardian.com/technology/2016/apr/19/ss7-hack-us-congressman-calls-texts-location-snooping), but still you want to be able to send and receive SMS.

## Requirements

`sms4you` is implemented as a [Python](https://www.python.org/) daemon. It relies on the [gammu](https://wammu.eu/gammu/) library for communication via SMS, and uses the libaries [imaplib](https://docs.python.org/3/library/imaplib.html) and [smtplib](https://docs.python.org/3/library/smtplib.html) to interact with an email address. For communicating with systemd, you need [sdnotify](https://github.com/bb4242/sdnotify).

## Installation

### Manually

* Install dependencies: `apt install gammu python3-gammu python3-pip python3-sdnotify python3-virtualenv`
* Download the code: `git clone https://github.com/xamanu/sms4you.git`
* Go into the new directory: `cd sms4you`
* Install python dependencies: `pip install -e .`
* Copy and edit the configuration file for sms4you: `cp config/sms4you-example /etc/sms4you/config`
* Copy and edit the configuration file for gammy: `cp config/gammu-example /etc/gammurc`
* Install the systemd service:
  `sudo cp sms4you.service /etc/systemd/system/`
  `sudo systemctl enable sms4you`
  `sudo systemctl start sms4you`

### Copyright and copyleft

Copyright (C) 2019  Felix Delattre

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Also add information on how to contact you by electronic and paper mail.

If your software can interact with users remotely through a computer
network, you should also make sure that it provides a way for users to
get its source.  For example, if your program is a web application, its
interface could display a "Source" link that leads users to an archive
of the code.  There are many ways you could offer source, and different
solutions will be better for different programs; see section 13 for the
specific requirements.

See the [LICENSE.md](./LICENSE.md) for the full license text.
