#!/usr/bin/python3

import argparse
import os
import sys
import time
from dotenv import load_dotenv
from .core.configuration import Configuration
from .core.gateway_factory import GatewayFactory

try:
    import sdnotify
except ImportError:
    sdnotify = None


def get_args():
    parser = argparse.ArgumentParser(
        prog="sms4you", description="Send and reveive SMS via an email account."
    )
    group = parser.add_mutually_exclusive_group()
    group.add_argument(
        "--you2sms",
        action="store_true",
        help="Only check for new emails and send out sms.",
    )
    group.add_argument(
        "--sms2you",
        action="store_true",
        help="Only check for new sms and send out emails.",
    )
    return parser.parse_args()


def main():
    load_dotenv(dotenv_path=".env", verbose=True)
    args = get_args()

    # Load, prepare and validate configuration
    config = Configuration()

    # Initiate gateways for communication through an object factory
    factory = GatewayFactory(config)
    sms_gateway = factory.get_sms_gateway()
    you_gateway = factory.get_you_gateway()

    # check, if we run as systemd service daemon or as a script
    is_daemon = os.getenv("INVOCATION_ID") is not None

    # after configuration is done, tell systemd, that we are ready
    if sdnotify is not None and is_daemon:
        n = sdnotify.SystemdNotifier()
        n.notify("READY=1")

    # Check if the flag to only run one direction is set.
    # If nothing is set, both will run (default behaviour).
    while True:
        if args.you2sms:
            _you2sms(sms_gateway, you_gateway)
        elif args.sms2you:
            _sms2you(sms_gateway, you_gateway)
        else:
            _sms2you(sms_gateway, you_gateway)
            _you2sms(sms_gateway, you_gateway)

        if not is_daemon:
            sys.exit()

        time.sleep(60)


def _you2sms(sms_gateway, you_gateway):

    # Check sms for new messages, loop through them and send one email
    # for each.
    for message in you_gateway.check():
        sms_gateway.send(message)


def _sms2you(sms_gateway, you_gateway):

    # Check emails for new messages, loop through them and send one
    # sms for each.
    for message in sms_gateway.check():
        you_gateway.send(message)


if __name__ == "__main__":
    main()
