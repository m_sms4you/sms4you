import os
import sys


class Configuration(object):
    """The Configuration class validates and prepares configuration data from
    the environment variables for further use in the script.

    """

    def get_environ(self, variable_name, default=""):
        try:
            variable = os.environ.get("SMS4YOU_" + variable_name, default)
            assert len(variable) > 0
        except (AssertionError, AttributeError):
            print(
                "Error: Required environment variable SMS4YOU_"
                + variable_name
                + " is not set",
                file=sys.stderr,
            )
            sys.exit()

        return variable

    def __init__(self):
        """Constructor function

        This function gets called when Configuration object are created.

        Based on the environment variables the configuration is validated and
        prepared all mandatory configuration elements.

        """

        # Currently only 'email' is supported. Other protocols such as
        # XMPP or matrix are to be implemented
        self.target_gateway = self.get_environ("TARGET_GATEWAY", "email")

        if self.target_gateway == "email":
            self.email_username = self.get_environ("EMAIL_USERNAME")
            with open(
                self.get_environ(
                    "EMAIL_PASSWORD_FILE", "/etc/sms4you/email_password.txt"
                )
            ) as f:
                self.email_password = f.read().strip()
            self.email_smtp_host = self.get_environ("SMTP_HOST")
            self.email_smtp_port = int(self.get_environ("SMTP_PORT", "465"))
            self.email_imap_host = self.get_environ("IMAP_HOST")
            self.email_imap_port = int(self.get_environ("IMAP_PORT", "993"))
            self.target_email = self.get_environ("TARGET_EMAIL")
        elif self.target_gateway == "xmpp":
            self.xmpp_component_jid = self.get_environ(
                "XMPP_COMPONENT_JID", "sms4you.localhost"
            )
            with open(
                self.get_environ(
                    "XMPP_COMPONENT_PASSWORD_FILE",
                    "/etc/sms4you/xmpp_component_password.txt",
                )
            ) as f:
                self.xmpp_component_password = f.read().strip()
            self.xmpp_server = self.get_environ("XMPP_SERVER", "localhost")
            self.xmpp_server_port = self.get_environ("XMPP_SERVER_PORT", "5347")
            self.xmpp_recipient_jid = self.get_environ("XMPP_RECIPIENT_JID")
        else:
            print(
                "Error: SMS4YOU_TARGET_GATEWAY mus be one of 'email' or 'xmpp'!",
                file=sys.stderr,
            )
